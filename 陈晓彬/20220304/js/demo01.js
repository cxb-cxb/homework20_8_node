/**
 * 作业布置
 * 
 * 优化 http 处理不同静态文件 css js image,放在不同的目录 css 放于 css 目录下
 * 
 * js放于 js 目录下，image 放于 image 下
 * 
 * 
 * mvc 是一种思想,一种设计模式
 * 
 * 
 *     express koa koa2 
 * 
 * 
 */

 let http=require('http');
 let fs=require('fs');
 
 let sever=http.createServer();
 
 sever.on('request',(req,res)=>{
     let url=req.url;
     console.log(url);
     if (req.url== './img') {
         res.end(fs.readFileSync('../img/1.jfif'))
     }
     if(req.url=='/css'){
         res.end(fs.readFileSync('../css/1.css'))
     }
     res.end()
 })
 sever.listen(8080)