var count = {
    add:function(num1,num2){
        return num1+num2;
    },
    subtract:function(num1 , num2){
        return num1 - num2;
    },
    ride:function(num1 , num2){
        return num1 * num2;
    },
    divide:function(num1,num2){
        return num1 /num2;
    }
}
function handle(num1,num2,symbol){
    if(isNaN(num1)||isNaN(num2)){
        throw Error("输入错误，请输入数字！！！");
    }
    return count[symbol](num1,num2)
}
module.exports = handle;