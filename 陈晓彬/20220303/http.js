let http = require("http");

let fs = require("fs");

let server=http.createServer();


server.on('request', (req, res) => {

    
    let url = req.url;

    if(url.indexOf("page")!=-1){
        let page=url.split("?")[1].split('=')[1]
        let bf=fs.readFileSync('./html/'+page+'.txt');
        res.write(bf);
    }
    res.end();
});

server.listen(80);