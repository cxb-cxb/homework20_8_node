//把课程代码都敲一敲，比较难理解，自己搭建一个web服务器，使用 mysql 的promise 封装，
//去实现一个动态页面，可以参看，课堂的代码，
let mysql = require('mysql');
let fs = require('fs');
let http = require('http');

http.createServer(function(req , res){
    if(req.url == '/a'){
        res.setHeader('Content-Type', 'text/html; charset=utf-8');
        let connection = mysql.createConnection({
            host: 'localhost', 
            user: 'root', 
            password: 'root', 
            port: '3306', 
            database: 'stuy' 
        });
        connection.connect();
        connection.query("select * from china where `date`=?",'2022-03-10',function(err,rows){
            let html=''
            for(let row in rows){
                   html+='<tr>'
                   html+='<td>'+ rows[row].city+'</td>'
                   html+='<td>'+ rows[row].nums+'</td>'
                   html+='<td>'+ rows[row].date+'</td>'
                   html+='</tr>'
            }            
            fs.readFile('./a.html',function(err,buffer){

                let outPutStr = buffer.toString();
                let newStr= outPutStr.replace('{{result}}',html);
                res.write(newStr);
                res.end();
            });  
        });
    
    }else{
        res.end();
    }

}).listen(8080)