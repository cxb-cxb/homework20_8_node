let http = require("http");
let fs = require("fs");

let server = http.createServer();
server.on('request',function(req , res){
    //获取到网站
    let url = http.url;

    //如果大于-1就是有点，是正确的。
    if(url.lastIndexOf(".") > -1){
        let deat ='.'+url;
        //判断有没有这个网站，如果有就读取这个文件在输出。
        if(fs.existsSync(deat)){
            let bf = fs.readFileSync(deat);
            res.write(bf);
        }
    }
    res.end("end");
})
server.listen(8030,function(){
    console.log("成功");
})